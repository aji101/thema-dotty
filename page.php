<?php
get_header();
?>

<article class="mt-10 ml-32 mr-32 text-white navbar h-auto">
<!-- AAA -->
<p class="text-center text-judul"><?= get_the_title();?></p>
<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();
				the_content();
				// get_template_part( 'template-parts/content/content' );
			}

			// // Previous/next page navigation.
			// twentynineteen_the_posts_navigation();

		} else {

			// If no content, include the "No posts found" template.
			// get_template_part( 'template-parts/content/content', 'none' );

		}
		?>

	    </article>

 <div style="position:static; bottom:0;">   
<?php 
get_footer();
?>
</div>