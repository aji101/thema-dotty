<?php
get_header();
?>

		<article class="content px-3 py-5 p-md-5">
<!-- AAA -->

<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();
				// get_template_part( 'template-parts/content/content' );
			}

			// // Previous/next page navigation.
			// twentynineteen_the_posts_navigation();

		} else {

			// If no content, include the "No posts found" template.
			// get_template_part( 'template-parts/content/content', 'none' );

		}
		?>

	    </article>

    
<?php 
get_footer();
?>