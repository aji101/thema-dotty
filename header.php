<!DOCTYPE html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Dotty Mind Communication and Coaching Sevices"> 
    <?php
    wp_head(); 
	include 'env.php';
    ?>
    <style>
        .dropdown {
            background-color: #0189ff;
            ;
        }

        body {
            background-color: #0D0D0D;
        }
    </style>
</head>

<body>
    <header x-data="{ mobileMenuOpen : false }"
        class="flex flex-wrap flex-row justify-between md:items-center md:space-x-4 bg-white py-6 px-6 fixed nav"
        style="z-index: 10000; background-color: #0D0D0D; position: fixed; top: 0;">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="block">
            <!-- <span class="sr-only">themes.dev</span> -->
            <img class="md:h-10 icon-dotty pl-10" src="<?php echo $loc_img ?>Group.png">
        </a>
        <button @click="mobileMenuOpen = !mobileMenuOpen" class="inline-block md:hidden w-8 h-8 burger">
            <svg fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clip-rule="evenodd"></path>
            </svg>
        </button>
        <nav class="absolute md:relative top-16 left-0 md:top-0 z-20 md:flex flex-col md:flex-row md:space-x-6 font-semibold w-full md:w-auto bg-white shadow-md rounded-lg md:rounded-none md:shadow-none md:bg-transparent p-6 pt-0 md:p-0"
            :class="{ 'flex' : mobileMenuOpen , 'hidden' : !mobileMenuOpen}" @click="mobileMenuOpen = false"
            style="background-color: #0D0D0D;">
            <li>
            <a v-bind:class="[activeClass]" id="css0" href="<?php echo esc_url( home_url( '/' ) ); ?>">HOME</a>
            </li>
            <li>
            <a v-bind:class="[activeClass]" id="css1" href="#REVIEW">REVIEW</a>
            </li>
            <li>
            <a v-bind:class="[activeClass]" id="css2" href="#PORTFOLIO">PORTFOLIO</a>
            </li>
            <?php	
					wp_nav_menu(
					array(
						'menu' => 'primary text-white',
						'container' => '',
						'theme_location'=> 'primary',
						'items_wrap' => '%3$s',
						'add_a_class' => 'text-white  hover-yellow rounded-lg block py-4 px-4 hover:text-white active:pt-32'
					)
				);
				?>
        </nav>
    </header>
    