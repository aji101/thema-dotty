<?php
get_header();
include 'env.php';
?>
    <div class="navbar" style="color: white;  z-index: 2; margin-left: 8%;">
        <div class="grid grid-cols-2">
            <div class="lg:mt-32">
                <h1 class="text-judul color-gradient">
                    Transforming
                </h1>
                <h2 class="text-judul color-gradient">
                    Your Mind!
                </h2>
                <div style="max-width: 70%;">
                    <p class="text-p">
                        We are Communication and Coaching Services, having a vision to help you achieve all the goals in
                        life by transforming your mind and thinking through the process of coaching techniques.
                    </p>
                </div>
                <div class="lg: mt-10" >
                    <a href="#">
                        <p class=" lg: p-4 butn-yellow text-p" id="SERVICES">
                            Read More
                            <img src="<?php echo $loc_img ?>arrow-right.png" class="arrow-right">
                        </p>
                    </a>

                </div>
            </div>
            <div>
                <div>
                <?php 
                    $original_query = $wp_query;
                    $wp_query = null;
                    $args=array('posts_per_page'=>5, 'tag' => 'gambar_bg');
                    $wp_query = new WP_Query( $args );
                    if ( have_posts() ) :
                        while (have_posts()) : the_post();
                        // the_post_thumbnail('thumbnail',array('class' => 'gambar'));
                        print_r(get_post_field('post_content', get_the_ID()));
                        endwhile;
                    endif;
                    $wp_query = null;
                    $wp_query = $original_query;
                    wp_reset_postdata();
                    ?>
                </div>

                <div >
                    <img class="pola-bulet" src="<?php echo $loc_img ?>Group 1.png">
                </div>
                <?php 
                    $original_query = $wp_query;
                    $wp_query = null;
                    $args=array('posts_per_page'=>5, 'tag' => 'gambar_otak');
                    $wp_query = new WP_Query( $args );
                    if ( have_posts() ) :
                        while (have_posts()) : the_post();
                        the_post_thumbnail('thumbnail',array('class' => 'icon-yellow'));
                        endwhile;
                    endif;
                    $wp_query = null;
                    $wp_query = $original_query;
                    wp_reset_postdata();
                    ?>
            </div>
        </div>
    </div>
    <section  >
    <div class="lg:mt-32" >
        <div>
            <p class="p-2 butn-yellow text-center mr-auto ml-auto" style="border: none;" >
                OUR SERVICE
            </p>
        </div>
        <div>
            <p class="text-white text-bjudul ml-auto mr-auto text-center mt-6">
                OUR AWESOME <span class="yellow"> SERVICE</span>
            </p>
        </div>
        <div>
        </div>
    </div>
    <div class="flex flex-col m-auto p-auto mt-20 ml-auto mr-auto xl:items-center ">
        <div class="flex overflow-x-scroll pb-10 hide-scroll-bar sm:show-scroll-bar ">
            <div class="flex flex-nowrap ">
                <div class="inline-block px-3">
                    <div class="card-gradient">
                        <img src="<?php echo $loc_img ?>pack1.svg" class="text-center ml-auto mr-auto pt-10 icon-height">
                        <p class="text-center yellow mt-4 mb-4">
                            Parenting Coaching
                        </p>
                        <p class=" text-center ml-4 mr-4" style="color: rgba(255, 255, 255, 1);">
                            We wanted to help parents have clarity in their parenting vision, and how to implement the values
                            they want to achieve and create greater communication among family members.
                        </p>
                    </div>
                </div>
                <div class="inline-block px-3">
                    <div class="card-gradient">
                        <img src="<?php echo $loc_img ?>pack2.svg" class="text-center ml-auto mr-auto pt-10 icon-height">
                        <p class="text-center yellow mt-4 mb-4">
                            Wellness Coaching
                        </p>
                        <p class=" text-center ml-4 mr-4" style="color: rgba(255, 255, 255, 1);">
                            We will help you discover your needs and how you can manage or plan to make you start living a
                            healthy life.
                        </p>
                    </div>
                </div>
                <div class="inline-block px-3">
                    <div class="card-gradient">
                        <img src="<?php echo $loc_img ?>pack3.svg" class="text-center ml-auto mr-auto pt-10 icon-height">
                        <p class="text-center yellow mt-4 mb-4">
                            Teens Coaching
                        </p>
                        <p class=" text-center ml-4 mr-4" style="color: rgba(255, 255, 255, 1);">
                            We wanted to help the teens to have clarity in their identity and discovering their talents.
                        </p>
                    </div>
                </div>
                <div class="inline-block px-3">
                    <div class="card-gradient">
                        <img src="<?php echo $loc_img ?>pack4.svg" class="text-center ml-auto mr-auto pt-10 icon-height">
                        <p class="text-center yellow mt-4 mb-4">
                            Entertainment Coaching
                        </p>
                        <p class=" text-center ml-4 mr-4" style="color: rgba(255, 255, 255, 1);">
                            We wanted to help them do more self-discovery, build their self-confidence, find their talents to do
                            more than just in the entertainment but expanding to another area as designed for them.
                        </p>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    </section>
  
  

    <div class="lg:mt-32 content2">
        <div style="display: block; float: left; width: 60%; margin-left: 7%;" class=" lg:mt-16">
            <p class="text-judul color-gradient">
                Transform your MIND and

            </p>
            <p class="text-judul color-gradient">
                Reaching your Goal
            </p>
            <p style="color: rgba(255, 255, 255, 1); width: 80%;">
                Sed perspiciatis unde omnis iste natus error sit voluptatem accusan tium doloremque laudantium, totam
                rem aperiam eaque ipsa quae abillo inventore veritatis et quasi architecto
            </p>
        </div>
        <div style="display: block; float: left; width: 30%;">
            <img  id="PORTFOLIO" src="<?php echo $loc_img ?>MIND_Final 1.png">
        </div>
    </div>
    <div class="mt-4" >
        <div>
            <p class="p-2 butn-yellow text-center mr-auto ml-auto" style="border: none;">
                PORTFOLIO
            </p>
        </div>
        <div>
            <p class="text-white text-cjudul ml-auto mr-auto text-center mt-6">
                More than 100+ client say this is a good <br> way to find your goal
            </p>
        </div>
        <div class="grid grid-cols-5 mt-10" id="REVIEW">
        <?php 
            $original_query = $wp_query;
            $wp_query = null;
            $args=array('posts_per_page'=>5, 'tag' => 'client');
            $wp_query = new WP_Query( $args );
            if ( have_posts() ) :
                while (have_posts()) : the_post();
                the_post_thumbnail('thumbnail',array('class' => 'ml-auto mr-auto'));
                endwhile;
            endif;
            $wp_query = null;
            $wp_query = $original_query;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="mt-16" >
        <div>
            <p class="p-2 butn-yellow text-center mr-auto ml-auto" style="border: none;">
                REVIEWS
            </p>
        </div>
        <p class="text-center text-cjudul text-white mt-6">
            Trusted by Our Clients
        </p>
        <div class="grid grid-cols-3 mt-16">
        <?php 
            $original_query = $wp_query;
            $wp_query = null;
            $args=array('posts_per_page'=>3, 'tag' => 'review');
            $wp_query = new WP_Query( $args );
            if ( have_posts() ) :
                while (have_posts()) : the_post();
                echo "<div class='mr-auto ml-auto' style='height: 200px; width: 80%;'>";
                echo "<div class='text-white'>";
                echo "<div class='text-comment' >" ; the_excerpt(); echo "</div>";
                echo "<div class='mt-4 text-center text-white text-mentor'>";
                echo " <div style='display: block; float: left; width: 20%;'>";
                the_post_thumbnail('thumbnail',array('class' => 'ml-auto mr-auto'));
                echo "</div>";
                echo " <div style='display: block; float: left; width: 80%;' class='mt-2'>";
                print_r(get_post_field('post_content', get_the_ID()));
                echo "</div></div></div></div>";
                endwhile;
            endif;
            $wp_query = null;
            $wp_query = $original_query;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="mt-52 sm:mt-20 text-center">
        <img class="ml-auto mr-auto" src="<?php echo $loc_img ?>Group 23.png">
    </div>
    <div class="mt-32">
        <img class="ml-auto mr-auto" src="<?php echo $loc_img ?>Rectangle 5.png">
    </div>
    <div class="mt-16">
        <p class="p-2 butn-yellow text-center mr-auto ml-auto" style="border: none;">
            MENTOR
        </p>
        <p class="mt-6 text-white text-cjudul text-center ">
            Meet Our Professional Coach
        </p>
    </div>
    <div class="mt-20">
        <div class="grid grid-cols-2">
        <?php 
            $original_query = $wp_query;
            $wp_query = null;
            $args=array('posts_per_page'=>2, 'tag' => 'mentor');
            $wp_query = new WP_Query( $args );
            if ( have_posts() ) :
                while (have_posts()) : the_post();
                echo "<div>";
                the_post_thumbnail('thumbnail',array('class' => 'ml-auto mr-auto'));
                echo "<div class='mt-4 text-center text-white text-mentor'>";
                print_r(get_post_field('post_content', get_the_ID()));
                echo "</div>";
                echo "</div>";
                endwhile;
            endif;
            $wp_query = null;
            $wp_query = $original_query;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="mt-16" style="position: relative;">
        <img class="lg:mr-auto lg:ml-auto lg:pl-32 lg:pr-32 lg:h-80 h-72" src="<?php echo $loc_img ?>Rectangle 12.png"
            style="z-index: 0; position: relative; width: 100%; ">
        <div class="lg:top-28 top-20" style="position: absolute;  z-index: 100;  width: 100%; ">
            <div class="lg:w-2/6 w-3/6 margin-left" style="display: block; float: left;">
                <p class="text-white text-judul">
                    You Can Be Your <br>
                        Own Coach!
                    </span> 
                </p>
            </div>
            <div class="lg:w-2/6 w-3/6 margin-right" style="display: block; float: left; ">
                <a class="text-right">
                    <p class="text-white text-djudul lg:mt-10 py-6 text-center"
                        style="background-color: #FED346; border-radius: 10px; width: 80%; ">
                        COLLABORATE WITH US
                    </p>
                </a>

            </div>
        </div>
    </div>
<?php
get_footer();
?>