
   <?php 
    wp_footer();
    include 'env.php';
    ?>
    <div class="mt-32 mb-32" id="ABOUT">
        <div class="grid lg:grid-cols-2 grid-cols-1">
            <div>
                <img class="lg:ml-20 ml-4 logo-size" src="<?php echo $loc_img?>LOGO_White 1.png">
                <p class="mt-4 text-white  lg:ml-20 ml-4" style="width: 60%;">
                    We are Communication and Coaching Services, having a vision to help you achieve all the goals in
                    life by transforming your mind and thinking through the process of coaching techniques.
                </p>
            </div>
            <div class="lg:ml-20 ml-4 lg:mt-4 mt-10">
                <div>
                    <a href="mailto:support@antardesa.id">
                        <p class="text-white mb-4">
                            <img src="<?php echo $loc_img?>mail.png" style="display: block; float: left;" class="mt-1 mr-4">
                            info@dottymind.com
                        </p>
                    </a>

                </div>

                <div>
                    <a
                        href="https://www.google.com/maps/dir/?api=1&destination=-6.17148%2C106.82649&fbclid=IwAR0tk_-ySmdQkzchQUMhyNRVRJlmd5X51XMvIPT3TF1YW2uCRae0kLn-ytk">
                        <p class="text-white mb-4">
                            <img src="<?php echo $loc_img?>map-pin.png" style="display: block; float: left; " class="mt-1 mr-4">
                            Address: Office 8, Level 18-A, Jl. Jend. Sudirman <span class="lg:ml-8 lg:flex">
                            Kav. 52-53 Kebayoran Baru, Jakarta Selatan 12190
                            </span>
                        </p>
                    </a>

                </div>

                <div>
                    <a href="https://api.whatsapp.com/send?phone=6282297771317">
                        <p class="text-white mb-4">
                            <img src="<?php echo $loc_img?>smartphone.png" style="display: block; float: left;" class="mt-1 mr-4">
                            +62 822-9777-1317
                        </p>
                    </a>

                </div>

                <div>
                    <a href="https://www.instagram.com/dotty.mind/">
                        <p class="text-white mb-4">
                            <img src="<?php echo $loc_img?>instagram.png" style="display: block; float: left;" class="mt-1 mr-4">
                            dotty.mind
                        </p>
                    </a>

                </div>

                <div>
                    <a href="https://web.facebook.com/DottyMind-Talk-103034351630926">
                        <p class="text-white mb-4">
                            <img src="<?php echo $loc_img?>facebook.png" style="display: block; float: left;" class="mt-1 mr-4">
                            dottymind talk
                        </p>
                    </a>

                </div>
            </div>
        </div>
    </div>
</body>


<script>
    var greenHover = 'textp-ml hover-yellow rounded-lg block py-4 px-4 hover:text-white text-white active:yellow';
    var app = new Vue({
        el: "#app",
        data: {
            message: "hello"
        }
    })
    for (var i = 0; i < 6; i++) {
        if (i == 0) {
            new Vue({
                el: "#css" + [i],
                variants: {
                        extend: {
                        backgroundColor: ['active'],
                        }
                    },
                data: {
                    activeClass: greenHover + "yellow",
                    message: "hello",
                },
                methods: {
                say: function (message) {
                        alert(message)
                        }
                    }
            })
        }
        new Vue({
            el: "#css" + [i],
            data: {
                activeClass: greenHover + "text-white"
            }
        })
    }
</script>
