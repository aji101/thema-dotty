<?php

function new_theme_support(){
    add_theme_support( 'title-tag' );
    add_theme_support( 'custom-logo' ,array(
	    'height'      => 100,
	    'width'       => 400,
	    'flex-height' => true,
	    'flex-width'  => true));
    add_theme_support( 'post-thumbnails' );
}

function new_theme_menu(){
    $locations = array(
        'primary' => "Desktop Primary Left Sidebar",
        'footer' => "Footer Menu Items"
    );
    register_nav_menus($locations);
}
function new_theme_register_style(){

    $version = wp_get_theme()->get('Version');
    wp_enqueue_style('new-theme-style',get_template_directory_uri()."/assets/css/style.css",array(),'all');
    wp_enqueue_style('new-theme-tailwind',get_template_directory_uri()."/assets/css/tailwind.min.css",array(),$version,'all');
    wp_enqueue_style('new-theme-fontawesome',get_template_directory_uri()."/assets/css/font-awesome.min.css",array(),$version,'all');
}

function new_theme_register_script(){
    wp_enqueue_script('new-theme-alphine','https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js',array(),'2.0.0',true);
    wp_enqueue_script('new-theme-vue',get_template_directory_uri().'/assets/js/vue.js',array(),'1.0',true);
}
function add_additional_class_on_a($classes, $item, $args)
{
    if (isset($args->add_a_class)) {
        $classes['class'] = $args->add_a_class;
    }
    return $classes;
}

add_filter('nav_menu_link_attributes', 'add_additional_class_on_a', 1, 3);
add_action('wp_enqueue_scripts', 'new_theme_register_style');
add_action('wp_enqueue_scripts', 'new_theme_register_script');
add_action('after_setup_theme','new_theme_support');
add_action('init','new_theme_menu');
?>